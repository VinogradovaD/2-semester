package Module_1;

import Jama.Matrix;

import java.util.Scanner;

public class Matrice1 {

    public static void main(String[] args) {
        double[] X = new double[4];
        double T,TT;
        Scanner in=new Scanner(System.in);
        System.out.println("Введите значения аргументов");
        for (int i=0; i<4; i++){
        X[i]=in.nextDouble();
        }
        New_arg y = new New_arg(new Testing());
        System.out.println("Возможный прогноз: "+y.f(X));


        New_arg Y = new New_arg(new Testing(){
            public double ex(double[][]ff, double[]yy, double [] X){
                double [] z;
                Matrix A1=new Matrix(ff);
                A1.print(5, 0);
                Matrix B1=A1.transpose();
                Matrix F1=A1.times(B1);
                Matrix F4=F1.inverse();
                Matrix F2=F4.times(A1);
                Matrix C=new Matrix(yy,5);
                Matrix F3=F2.times(C);
                z=F3.getColumnPackedCopy();
                for (int i = 0; i < 5; i++) {
                    System.out.println("z[" + i + "]=" + z[i]);
                }
                double Y =z[0]+z[1]*X[0]+z[2]*Math.log(Math.pow(X[1],2))+z[3]*Math.log(X[2])+z[4]*X[3];
                return Y;
            }
        });
        New_arg D = new New_arg(new Testing(){
            public double ex(double[][]ff, double[]yy, double [] X){
                double [] z;
                Matrix A1=new Matrix(ff);
                A1.print(5, 0);
                Matrix B1=A1.transpose();
                Matrix F1=A1.times(B1);
                Matrix F4=F1.inverse();
                Matrix F2=F4.times(A1);
                Matrix C=new Matrix(yy,5);
                Matrix F3=F2.times(C);
                z=F3.getColumnPackedCopy();
                for (int i = 0; i < 5; i++) {
                    System.out.println("z[" + i + "]=" + z[i]);
                }
                double Y =z[0]+z[1]*X[0]+z[2]*X[1]+z[3]*X[2]+z[4]*X[3];
                return Y;
            }
        });
        T=Y.f(X);
        System.out.println("Погрешность: " + T);
        TT=D.f(X);
        System.out.println("Погрешность: " + TT);

    }

}